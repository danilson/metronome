/** \file ClickerRandomMute.java */

package com.arnaud.metronome;

import android.content.Context;
import android.media.AudioTrack;

import android.os.AsyncTask;
import android.os.Build;//Same reason as in Clicker (for Polyrhythms). We need several asynctask
import java.util.Random;
import java.lang.Integer;//Only for MAX_VALUE (=2^31-1)
import android.os.SystemClock; //For sleeping

//import java.lang.Math;
//import android.util.Log;

/** It's basically a Clicker who launch a muter asyntask + the classic run asynctask */
public class ClickerRandomMute extends Clicker
{
	int freqMean, freqDev, lengthMean, lengthDev;
	private Random r;
	
	protected RandomMuteTask randomMuteTask;

	public ClickerRandomMute (ContainerRandomMute container, double bpm, double bar, Context context)
	{
		super(bpm,bar,context);
		this.freqMean = container.freqMean;
		this.freqDev = container.freqDev;
		this.lengthMean = container.lengthMean;
		this.lengthDev = container.lengthDev;
		
		r = new Random();
	}

	/* The task handling Clicker's main loop. */
	private class RandomMuteTask extends AsyncTask<Void, Void, Void> {
		protected Void doInBackground(Void... params) {randomMute(); return null;}
	}

	@Override
	public void start()
	{//It's not that important to launch the muter asap.
		randomMuteTask = new RandomMuteTask();
		super.start();
		if(Build.VERSION.SDK_INT >= 11)
			randomMuteTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			randomMuteTask.execute();
	}

	@Override
	public void stop()
	{
		super.stop();//We need the isRunning kill switch before canceling the mute task. Or else, it goes forever and take a thread for nothing.
		randomMuteTask.cancel(true);
	}

	protected void randomMute()
	{//We could use a transformation between unifomr to normal(gaussian) distribution but there is already a gaussian distrib so...
		double freq, length;

		while (isRunning) {
		//The ifs are redundant since means and deviances won't change. If deviances are 0, we can set freq = freqmean once and for all. Problem is I can't see how to do it with having 4 different while loops.
			if (freqDev == 0)//deterministic
				freq = freqMean;
			else {//random
				freq = r.nextGaussian()*freqDev+freqMean;
				if (freq < 0)//We can't mute past sounds!
					freq = 0;
			}
			if (lengthDev == 0)
				length = lengthMean;
			else {
				length = r.nextGaussian()*lengthDev+lengthMean;
				if (length < 0)
					length = 0;
			}
			//We can't feed int > MAX_VALUE (=MAX_INT). Considering that freqMean+freqDev could be as high as 2*MAX_INT, we are feeding 2*1000*MAX_INT to SystemClock.sleep(). Let's avoid that
			freq *= 1000;//Convert to ms
			length *= 1000;
			if (freq > Integer.MAX_VALUE)
				freq = Integer.MAX_VALUE;
			if (length > Integer.MAX_VALUE)
				length = Integer.MAX_VALUE;
			
			//We do some check here because we don't want to occupy a thread for several sec after the user stopped the click
			if (!isRunning)
				return;
			SystemClock.sleep((int) freq);
			audioTrack.setStereoVolume(0,0);
			if (!isRunning)
				return;
			SystemClock.sleep((int) length);
			audioTrack.setStereoVolume(1,1);
		}
	}
}
