/** \file ClickerStructure.java */

package com.arnaud.metronome;

import android.content.Context;
import android.os.AsyncTask;

import android.media.AudioTrack;
//import android.util.Log;

/** It's basically a Clicker who travel through a structure while settings signal and while clicking */
public class ClickerStructure extends Clicker
{
	private Structure structure;
	private Pattern[] patterns;
	private Base[] bases;
	
	public ClickerStructure (ContainerStructures container, int structureIndex, double bpm, double bar, Context context)
	{//Mandatory call to parent constructor (implicit or not). So I have to craft the classes depending on it.
		super(bpm,bar,context);
		this.structure = container.structures[structureIndex];
		this.patterns = container.patterns;
		this.bases = container.bases;
		initSignal2();
	}

	@Override
	protected void initSignal()
	{//We want to empty this method because it is called by Clicker() and is useless in ClickerStructure. Since we can't initialised our variable before caling the constructor, we need to neuterize this thing and use another method. Or else segfault because unitialized variables
	//Not hacky at all...
	}

	protected void initSignal2() {
		double structureSpeed = bpm;

		/*Set Sounds*/
		for(int i=0;i<structure.patterns.length;i++) {
			int patternIndex = structure.patterns[i].index;
			if (!patterns[patternIndex].isSetup) {
				for(int j=0;j<patterns[patternIndex].bases.length;j++) {
					int baseIndex = patterns[patternIndex].bases[j].index;
					if (!bases[baseIndex].isSetup) {
						double barBpm = bases[baseIndex].bpm * structureSpeed/100;//This allows control of global speed accross the structure with the global bpm (as a percentage).
						if (bases[baseIndex].ticChoice == -1)
							bases[baseIndex].tic = Signal.getSignal(context,ticChoice,ticFreq,ticLength,sampleRate,barBpm);
						else
							bases[baseIndex].tic = Signal.getSignal(context,bases[baseIndex].ticChoice,ticFreq,ticLength,sampleRate,barBpm);
						if (bases[baseIndex].tocChoice == -1)
							bases[baseIndex].toc = Signal.getSignal(context,tocChoice,tocFreq,tocLength,sampleRate,barBpm);
						else
							bases[baseIndex].toc = Signal.getSignal(context,bases[baseIndex].tocChoice,tocFreq,tocLength,sampleRate,barBpm);
						bases[baseIndex].isSetup = true;
					}
				}
				patterns[patternIndex].isSetup = true;
			}
		}
	}

	@Override
	public void run()
	{//We need to check the isRunning kill switch as several place because a structure ca be long (or can use exotic bar values) and the method may occupy a thread for nothing long after the user stopped.
		byte[] tic;
		byte[] toc;
		double bar;
		
		int structureIter = (int) this.bar;//To deal with non integer value
		if (structureIter == 0)//infinite loop
			structureIter = -1;
		
		while(structureIter != 0 && isRunning) {
			for(int i=0;i<structure.patterns.length;i++) {
			int patternIndex = structure.patterns[i].index;
			int patternTimes = structure.patterns[i].times;
			for(int j=0;j<patternTimes;j++) {
				for(int k=0;k<patterns[patternIndex].bases.length;k++) {
				int baseIndex = patterns[patternIndex].bases[k].index;
				int baseTimes = patterns[patternIndex].bases[k].times;
				tic = bases[baseIndex].tic;
				toc = bases[baseIndex].toc;
				bar = bases[baseIndex].bar;
				for(int l=0;l<baseTimes;l++) {
				int beat = 0;
				if (bar == 1.) //Play only tic
					audioTrack.write(tic,0,tic.length);
				else if (bar == 0.) //Play only toc
					audioTrack.write(toc,0,toc.length);		
				else if (bar%1 == 0.)//bar is "int". Common use.
					while(beat < bar && isRunning) {
						if (beat == 0)
							audioTrack.write(tic,0,tic.length);
						else
							audioTrack.write(toc,0,toc.length);
						beat++;
					}
					else if (bar > 1.) { //bar is dec >1
						int partial = (int) ((toc.length)*(bar%1));
						audioTrack.write(tic,0,tic.length);
						beat++;
						while(beat < bar && isRunning) {
							if (beat < (int) bar)//Common beat
								audioTrack.write(toc,0,toc.length);
							else
								audioTrack.write(toc,0,partial);
							beat++;
						}
					}
					else {
						int partial = (int) ((tic.length)*bar);
						audioTrack.write(tic,0,partial);
					}
				}
				}
			}
			}
			structureIter--;
		}
	}
}
