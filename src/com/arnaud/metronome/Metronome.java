/** \mainpage Presentation
* Metronome.java is the main class. When Start is pressed, it calls Clicker.java, ClickerStructure.java or ClickerPolyrhythms.java depending on the mode chosen. The sounds arrays are initialisd through Signal. Then the main loop is run in a separate thread.<br>
* MenuSettings.java uses SharedPreferences so the 3 clickers classes can set the audioTrack and their signal from it.<br>
* MenuStructures.java and MenuPolyrhythms.java exchange data wich Metronome.java through intents.<br>
* ContainerStructures|Polyrhythms.java is a commodity class that encapsulates all the data for structures (resp. polyrhythms) and deals with I/O (through intent and disk r/w).
*/

/** \file Metronome.java
* Main class.
*/

package com.arnaud.metronome;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;

import android.media.AudioManager; //Volume control through hardware button
import android.content.Intent; //Pour ouvrir d'autres activity: settings/pattern
import android.widget.Toast; //Pour afficher des trucs
import android.os.SystemClock; //Pour chronometrer le Tap In
import java.util.List;
import java.util.ArrayList;

/*Menu*/
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
/*UI*/
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;

//import android.util.Log;

/** Main class
*/
public class Metronome extends Activity
{
	/* UI */
	private EditText editTextBpm;
	private EditText editTextBar;
	private Spinner spinnerMode;
	private ImageButton imageButtonStartStop;
	List<String> listMode = new ArrayList<String>();

	/* Data Structures */
	private ContainerStructures containerStructures;
	private ContainerPolyrhythms containerPolyrhythms;
	private ContainerRandomMute containerRandomMute;

	/* Clickers */
	private Clicker clicker;
	private ClickerRandomMute clickerRandomMute;
	private ClickerPolyrhythms clickerPolyrhythms;
	private ClickerStructure clickerStructure;

	/* States */
	private long lastTapIn; /**< needed to compute the bpm from tapIn*/
	private int mode; /**< Current running mode. 0: normal clicker, 1: randommute, 2: polyrhythms, >2: structure clicker */
	private boolean isRunning;/**< Commands what to do when startStop is pressed*/
	private double[] bpm;
	private double[] bar;

	/* Constantes */
	/* Les IDs pour le menu */
	private final int ID_MENU_SETTINGS = 1;
	private final int ID_MENU_STRUCTURES = 2;
	private final int ID_MENU_POLYRHYTHMS = 3;
	private final int ID_MENU_RANDOMMUTE = 4;
	private final int ID_MENU_ABOUT = 5;
	/* Run Mode */
	private final int MODE_NORMAL = 0; /**< Basic mode. Clicker.java used. */
	private final int MODE_RANDOMMUTE = 1; /**< Mute mode. The class uses the basis of clicker + a muter task over it . */
	private final int MODE_POLYRHYTHMS = 2; /**< Polyrhythms mode. Several Clicker.java used. If >2 it means structures. */
	private final int MODE_COUNT = 4; /** Number of modes, not differentiating structures. We use it to avoid segfault when mode point to 2nd structure and above */

	/* Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//Build UI and enable volume control
		editTextBpm = (EditText) findViewById(R.id.editTextBpm);
		editTextBar = (EditText) findViewById(R.id.editTextBar);
		spinnerMode = (Spinner) findViewById(R.id.spinnerMode);
		imageButtonStartStop = (ImageButton) findViewById(R.id.imageButtonStartStop);

		ContainerStructures tmp = new ContainerStructures();
		containerStructures = tmp.loadFromInternal(this);
		ContainerPolyrhythms tmp2 = new ContainerPolyrhythms();
		containerPolyrhythms = tmp2.loadFromInternal(this);
		ContainerRandomMute tmp3 = new ContainerRandomMute();
		containerRandomMute = tmp3.loadFromInternal(this);
		
		int structuresCount = containerStructures.structures.length;
		listMode.add(getResources().getString(R.string.listMode_normal));
		listMode.add(getResources().getString(R.string.listMode_randomMute));
		listMode.add(getResources().getString(R.string.listMode_polyrhythms));
		for (int i=1;i<structuresCount+1;i++)
			listMode.add(String.format(getResources().getString(R.string.listMode_structure),i));
		ArrayAdapter<String> adapterSpinnerMode = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listMode);
		adapterSpinnerMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerMode.setAdapter(adapterSpinnerMode);
		spinnerMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
				/** This method is automatically called somewhere after onCreate() even if nothing moves. So the editText are reset to default values instead of the sharedprefs ones. That's why we need a flag that flip after the first call and the method only proceed if the flag = true. Broken spinner!!*/
				if (position >= MODE_COUNT) {
				//Set values as structure values
					editTextBpm.setText(String.valueOf(bpm[MODE_COUNT-1]));
					editTextBar.setText(String.valueOf(bar[MODE_COUNT-1]));
				}
				else {
					editTextBpm.setText(String.valueOf(bpm[position]));
					editTextBar.setText(String.valueOf(bar[position]));
				}
				
				if (position >= MODE_POLYRHYTHMS) {
					Toast.makeText(getApplicationContext(),R.string.toastMode,Toast.LENGTH_LONG).show();
				}
				/**We don't want to change mode here. If using the spinner while a clicker is running, you would stop() the wrong class. Instead, mode is set only before a clicker is started and so, we can stop the right instance by keeping the value in mode unchanged independently from the spinner.*/
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		SharedPreferences settings = getSharedPreferences("settings", 0);
		if (settings.getBoolean("isScreenAlwaysOn",false) == true)
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		else
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mode = settings.getInt("listMode", MODE_NORMAL);
		spinnerMode.setSelection(mode); /* Useless? */
		isRunning = false;
		bpm = new double[MODE_COUNT];
		bar = new double[MODE_COUNT];
		bpm[0] = Double.longBitsToDouble(settings.getLong("bpm_normal", Double.doubleToLongBits(120)));
		bar[0] = Double.longBitsToDouble(settings.getLong("bar_normal", Double.doubleToLongBits(4)));
		bpm[1] = Double.longBitsToDouble(settings.getLong("bpm_randommute", Double.doubleToLongBits(120)));
		bar[1] = Double.longBitsToDouble(settings.getLong("bar_randommute", Double.doubleToLongBits(4)));
		bpm[2] = Double.longBitsToDouble(settings.getLong("bpm_polyrhythms", Double.doubleToLongBits(100)));
		bar[2] = Double.longBitsToDouble(settings.getLong("bar_polyrhythms", Double.doubleToLongBits(0)));
		bpm[3] = Double.longBitsToDouble(settings.getLong("bpm_structures", Double.doubleToLongBits(100)));
		bar[3] = Double.longBitsToDouble(settings.getLong("bar_structures", Double.doubleToLongBits(0)));
		if (mode >= MODE_COUNT) {
		/* If the 2nd structure is selected, bpm[position] is out of boundary so we clip to MODE_COUNT-1 (=sizeof(bpm or bar)) */
			editTextBpm.setText(String.valueOf(bpm[MODE_COUNT-1]));
			editTextBar.setText(String.valueOf(bar[MODE_COUNT-1]));
		}
		else {
			editTextBpm.setText(String.valueOf(bpm[mode]));
			editTextBar.setText(String.valueOf(bar[mode]));
		}
	}

	/** Handle the back button
	* just call the onQuit() method wich handles the saves
	*/
	@Override
	public void onBackPressed()
	{
		onQuit(null);//The method isn't called from a widget
	}

	/** Build the menu
	* menu.xml show an empty so we fille it ourself
	*/
	@Override
	public boolean onCreateOptionsMenu (Menu menu)
	{
		/*
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu,menu);
		*/
		
		//menu.add(group.id,item id,order,title)
		menu.add(Menu.NONE,ID_MENU_RANDOMMUTE,Menu.NONE,R.string.menuRandomMute);
		menu.add(Menu.NONE,ID_MENU_POLYRHYTHMS,Menu.NONE,R.string.menuPolyrhythms);
		menu.add(Menu.NONE,ID_MENU_STRUCTURES,Menu.NONE,R.string.menuStructures);
		menu.add(Menu.NONE,ID_MENU_SETTINGS,Menu.NONE,R.string.menuSettings);
		menu.add(Menu.NONE,ID_MENU_ABOUT,Menu.NONE,R.string.menuAbout);
		return true;
	}
	
	/** Set new activities for options, feed them current data */
	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
	switch (item.getItemId()) {
		case ID_MENU_RANDOMMUTE:
			Intent intentMenuRandomMute = new Intent(Metronome.this, MenuRandomMute.class);
			intentMenuRandomMute.putExtra("containerRandomMute",containerRandomMute);
			startActivityForResult(intentMenuRandomMute,ID_MENU_RANDOMMUTE);
			return true;
		case ID_MENU_POLYRHYTHMS:
			Intent intentMenuPolyrhythms = new Intent(Metronome.this, MenuPolyrhythms.class);
			intentMenuPolyrhythms.putExtra("containerPolyrhythms",containerPolyrhythms);
			startActivityForResult(intentMenuPolyrhythms,ID_MENU_POLYRHYTHMS);
			return true;
		case ID_MENU_STRUCTURES:
			Intent intentMenuStructures = new Intent(Metronome.this, MenuStructures.class);
			intentMenuStructures.putExtra("containerStructures",containerStructures);
			startActivityForResult(intentMenuStructures,ID_MENU_STRUCTURES);
			return true;
		case ID_MENU_SETTINGS:
			Intent intentMenuSettings = new Intent(Metronome.this, MenuSettings.class);
			startActivityForResult(intentMenuSettings,ID_MENU_SETTINGS);
			return true;
		case ID_MENU_ABOUT:
			Intent intentMenuAbout = new Intent(Metronome.this, MenuAbout.class);
			startActivity(intentMenuAbout);
			return true;
		default:
			return super.onOptionsItemSelected(item);
	}
	}
	/** And grab back edited data. MenuSettings won't return any intent. */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case ID_MENU_RANDOMMUTE:
				if(resultCode == 1) {
					containerRandomMute = (ContainerRandomMute)data.getSerializableExtra("containerRandomMute");
					spinnerMode.setSelection(MODE_RANDOMMUTE);//Select randommute
				}
			return;
			case ID_MENU_POLYRHYTHMS:
				if(resultCode == 1) {
					containerPolyrhythms = (ContainerPolyrhythms)data.getSerializableExtra("containerPolyrhythms");
					int polyrhythmsCount = containerPolyrhythms.rhythms.length;
					if (polyrhythmsCount != 0)
						spinnerMode.setSelection(MODE_POLYRHYTHMS);//Select polyrhythms
					else
						spinnerMode.setSelection(MODE_NORMAL);//In case empty polyrhythms
				}
			return;
			case ID_MENU_STRUCTURES:
				if(resultCode == 1) {
					containerStructures = (ContainerStructures)data.getSerializableExtra("containerStructures");
					int structuresCount = containerStructures.structures.length;
					listMode.clear();
					listMode.add(getResources().getString(R.string.listMode_normal));
					listMode.add(getResources().getString(R.string.listMode_randomMute));
					listMode.add(getResources().getString(R.string.listMode_polyrhythms));
					if (structuresCount != 0) {
						spinnerMode.setSelection(MODE_POLYRHYTHMS+1);//Selection 1st structure
						for(int i=1;i<structuresCount+1;i++)
							listMode.add(String.format(getResources().getString(R.string.listMode_structure),i));
					}
					else
						spinnerMode.setSelection(MODE_NORMAL);//In case the previously selected structure has been deleted.
				}
			return;
			case ID_MENU_SETTINGS:
				if (resultCode == 1) {
					SharedPreferences settings = getSharedPreferences("settings", 0);
					if (settings.getBoolean("isScreenAlwaysOn",false) == true)
						getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					else
						getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				}
			return;
		}
	}

	/* Buttons Handlers. */
	public void onTapIn (View v)
	{// If using SystemClock.elapsedRealtimeNanos() (API 17), you need to adapt marked lines.
		long delay, currentTapIn;
		double bpm;
		
		currentTapIn = SystemClock.elapsedRealtime();//<--
		delay = currentTapIn - lastTapIn;
		lastTapIn = currentTapIn;

		bpm = 60000./delay;//<--60000000000/delay if nanotime
		editTextBpm.setText(String.valueOf(bpm));
		//We don't do the "clear focus dance" (see onStartStop) do avoid bloat.
	}
	
	/** Toggle the clicker depending on the current state \var mode */
	public void onStartStop (View v)
	{
	if (!isRunning) {//Go!
		/** UI stuff */
		String stringBpm = editTextBpm.getText().toString();
		String stringBar = editTextBar.getText().toString();
		if (stringBpm.matches("") || stringBar.matches("")) {
			Toast.makeText(getApplicationContext(),R.string.toastEnterSomeValue,Toast.LENGTH_SHORT).show();
			return;
		}
		
		double bpm_tmp = Double.parseDouble(stringBpm);
		double bar_tmp = Double.parseDouble(stringBar);
		if (bpm_tmp <= 0) {
			Toast.makeText(getApplicationContext(),R.string.toastBpmCantBeZero,Toast.LENGTH_SHORT).show();
			return;
		}

		// Set variables from UI
		mode = spinnerMode.getSelectedItemPosition();
		if (mode >= MODE_COUNT) {
		/* If the 2nd structure is selected, bpm[position] is out of boundary so we clip to MODE_COUNT-1 (=sizeof(bpm or bar)) */
			bpm[MODE_COUNT-1] = bpm_tmp;
			bar[MODE_COUNT-1] = bar_tmp;
		}
		else {
			bpm[mode] = bpm_tmp;
			bar[mode] = bar_tmp;
		}

		/** Clicker Stuff */
		//Concerning the outofmemory, instead of doing the checks in Signal, return null if failed and propagate back to the clicker* then metronome, I chose to simply do it here. One of the reason is that java can't return null from the constructor so in order to check for failure, I should use a public var in my class (initSuccess ?) that I check here ?
		if (mode == MODE_NORMAL) {//Normal Clicker
			try {
				clicker = new Clicker(bpm[MODE_NORMAL],bar[MODE_NORMAL],getApplicationContext());
			} catch (OutOfMemoryError e) {
				Toast.makeText(getApplicationContext(),R.string.toastOutOfMemory,Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return;
			}
			clicker.start();
		}
		else if (mode == MODE_RANDOMMUTE) {
			try {
				clickerRandomMute = new ClickerRandomMute(containerRandomMute,bpm[MODE_RANDOMMUTE],bar[MODE_RANDOMMUTE],getApplicationContext());
			} catch (OutOfMemoryError e) {
				Toast.makeText(getApplicationContext(),R.string.toastOutOfMemory,Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return;
			}
			clickerRandomMute.start();
		}
		else if (mode == MODE_POLYRHYTHMS) {//Polyrhythms Clicker
			try {
				clickerPolyrhythms = new ClickerPolyrhythms(containerPolyrhythms,bpm[MODE_POLYRHYTHMS],bar[MODE_POLYRHYTHMS],getApplicationContext());
			} catch (OutOfMemoryError e) {
				Toast.makeText(getApplicationContext(),R.string.toastOutOfMemory,Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return;
			}
			clickerPolyrhythms.start();
		}
		else {//Structure Clicker
			try {
				clickerStructure = new ClickerStructure(containerStructures,mode-3,bpm[MODE_COUNT-1],bar[MODE_COUNT-1],getApplicationContext());
			} catch (OutOfMemoryError e) {
				Toast.makeText(getApplicationContext(),R.string.toastOutOfMemory,Toast.LENGTH_SHORT).show();
				e.printStackTrace();
				return;
			}
			clickerStructure.start();
		}

		isRunning = true;

		imageButtonStartStop.setImageResource(android.R.drawable.ic_media_pause);
		imageButtonStartStop.setBackgroundColor(0xFFFF0000);
		//Clear focus dance
		//Make the keyboard disappear
		editTextBpm.setEnabled(false);
		editTextBpm.setEnabled(true);
		//Clear the cursor. So the nect time we touch it, it will selectAll. clearFocus() actually move focus to the first focusable view (that used to be editTextBpm) so editTextBpm would be focused each time something called clearFocus() (exactly what we want to avoid!!). By setting the parent layout focusable(intouchmode), the focus is properly removed (moved to the layout).
		editTextBpm.clearFocus();
		editTextBar.setEnabled(false);
		editTextBar.setEnabled(true);
		editTextBar.clearFocus();
	}
	else {//Stop!
		//The spinnerMode is only read when starting so even if we changed the spinner selection while clicking, mode is still unchanged on stop. That's why we are able to determine the class to shutdown between clicker or clickerStructure.
		if (mode == MODE_NORMAL) {
			clicker.stop();
		}
		else if (mode == MODE_RANDOMMUTE) {
			clickerRandomMute.stop();
		}
		else if (mode == MODE_POLYRHYTHMS) {
			clickerPolyrhythms.stop();
		}
		else {
			clickerStructure.stop();
			containerStructures.resetAudio();//Or else an eventual bpm change wont be computed.
		}

		isRunning = false;

		imageButtonStartStop.setImageResource(android.R.drawable.ic_media_play);
		imageButtonStartStop.setBackgroundColor(0xFF00FF00);
		//Clear focus
		editTextBpm.setEnabled(false);
		editTextBpm.setEnabled(true);
		editTextBpm.clearFocus();
		editTextBar.setEnabled(false);
		editTextBar.setEnabled(true);
		editTextBar.clearFocus();
	}

	return;
	}

	public void onQuit (View v)
	{	
		SharedPreferences settings = getSharedPreferences("settings", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong("bpm_normal",Double.doubleToRawLongBits(bpm[MODE_NORMAL]));
		editor.putLong("bar_normal",Double.doubleToRawLongBits(bar[MODE_NORMAL]));
		editor.putLong("bpm_randommute",Double.doubleToRawLongBits(bpm[MODE_RANDOMMUTE]));
		editor.putLong("bar_randommute",Double.doubleToRawLongBits(bar[MODE_RANDOMMUTE]));
		editor.putLong("bpm_polyrhythms",Double.doubleToRawLongBits(bpm[MODE_POLYRHYTHMS]));
		editor.putLong("bar_polyrhythms",Double.doubleToRawLongBits(bar[MODE_POLYRHYTHMS]));
		editor.putLong("bpm_structures",Double.doubleToRawLongBits(bpm[MODE_COUNT-1]));
		editor.putLong("bar_structures",Double.doubleToRawLongBits(bar[MODE_COUNT-1]));
		editor.putInt("listMode",spinnerMode.getSelectedItemPosition());
		editor.commit();
		finish();
		System.exit(0);
	}
}
