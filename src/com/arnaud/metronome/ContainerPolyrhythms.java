/** \file ContainerPolyrhythms.java */

package com.arnaud.metronome;

import android.content.Context;

import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

/** This is very similar to MenuStructures.java.
* It's a dumbed down version */

class Rhythm implements Serializable {
	double bpm;
	double bar;
	int ticChoice = -1;
	int tocChoice = -1;
}

class ContainerPolyrhythms implements Serializable {
	private String FILENAME = "polyrhythms";

	Rhythm[] rhythms = new Rhythm[0];

	/** Internal memory means not sdcard and no permissions headaches needed */
	public void saveToInternal (Context context)
	{
		try {//Forcé de faire ça. Java c'est reloud.
			FileOutputStream fos = context.openFileOutput(FILENAME, context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ContainerPolyrhythms loadFromInternal (Context context)
	{
		ContainerPolyrhythms container = new ContainerPolyrhythms();
		
		try {
			FileInputStream fis = context.openFileInput(FILENAME);
			ObjectInputStream ois = new ObjectInputStream(fis);
			container = (ContainerPolyrhythms) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return container;
	}
}
