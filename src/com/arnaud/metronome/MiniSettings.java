/** \file MiniSettings.java. StructuresBases and Polyrhythms' rhythms Settings activity */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;
import java.lang.Exception;

import android.content.SharedPreferences;
import android.widget.Toast;
/*UI*/
import android.view.View;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import java.util.List;
import java.util.ArrayList;//il me faut une liste pour le spinner juste pour rajouter generated et muted. On peut pas étendre un tableau java!!! C'est toujours mieux que d'avoir la liste en dur dans string-array strings.xml
import java.lang.reflect.Field;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.CheckBox;

import android.content.Intent;

//import android.util.Log;

/** MiniSettings inherits from MenuSettings and extends or overridden some parts to accomodate.
* It's an activity that allow a convinient way to choose wich sounds to use for each base|rhythm.
*/

public class MiniSettings extends MenuSettings
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		((LinearLayout) findViewById(R.id.menuSettingsLayoutMisc)).setVisibility(View.GONE);
		CheckBox ticCheckBox = ((CheckBox) findViewById(R.id.miniSettingsCheckBoxTic));
		CheckBox tocCheckBox = ((CheckBox) findViewById(R.id.miniSettingsCheckBoxToc));
		ticCheckBox.setVisibility(View.VISIBLE);
		tocCheckBox.setVisibility(View.VISIBLE);
		onCheckBox(ticCheckBox);
		onCheckBox(tocCheckBox);
	}

	@Override
	public void loadSettings()
	{
		super.loadSettings();//We want to load some values at least even if never displayed.
		Intent intent = getIntent();
		tic = intent.getIntExtra("tic",-1);
		toc = intent.getIntExtra("toc",-1);

		if (tic == -1)
			((CheckBox) findViewById(R.id.miniSettingsCheckBoxTic)).setChecked(true);
		else
			spinnerTic.setSelection(tic);

		if (toc == -1)
			((CheckBox) findViewById(R.id.miniSettingsCheckBoxToc)).setChecked(true);
		else
			spinnerToc.setSelection(toc);
		}

	@Override
	public void saveSettings()
	{
		if (((CheckBox) findViewById(R.id.miniSettingsCheckBoxTic)).isChecked())
			tic = -1;
		if (((CheckBox) findViewById(R.id.miniSettingsCheckBoxToc)).isChecked())
			toc = -1;

		Intent intent = new Intent();
		intent.putExtra("tic",tic);
		intent.putExtra("toc",toc);
		setResult(1,intent);
		finish();
	}
	
	@Override
	public void onMenuSettingsCancel (View v)
	{
		setResult(0,null);
		finish();
	}

	public void onCheckBox(View v)
	{
		CheckBox clicCheckBox;
		LinearLayout clicLayout;
		if (v.getTag().toString().equals("tic")) {
			clicCheckBox = (CheckBox) findViewById(R.id.miniSettingsCheckBoxTic);
			clicLayout = (LinearLayout) findViewById(R.id.menuSettingsLayoutTic);
		}
		else if (v.getTag().toString().equals("toc")) {
			clicCheckBox = (CheckBox) findViewById(R.id.miniSettingsCheckBoxToc);
			clicLayout = (LinearLayout) findViewById(R.id.menuSettingsLayoutToc);
		}
		else { //compiler says this might be unitisialised so we put a default
			clicCheckBox = (CheckBox) findViewById(R.id.miniSettingsCheckBoxTic);
			clicLayout = (LinearLayout) findViewById(R.id.menuSettingsLayoutTic);
		}

		if (clicCheckBox.isChecked())
			clicLayout.setVisibility(View.GONE);
		else
			clicLayout.setVisibility(View.VISIBLE);
		}
}
