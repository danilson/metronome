# Metronome

Metronome is a free and open-source Android app reliable, with large BPM capabilites and able to follow a user defined structure, polyrhythm and randomized mute.

## Features

The purpose of Metronome is to bring together several features in one app:

* free and open-source
* BPM ranging from ~0.2 to ~120000 (depending on phone)
* can handle floating-point bars
* reliable "clicker". This metronome is as regular as playing a pre-recorded metronome mp3
* possibility to define a complex structure (metric changes, various pattern, bpm variation)
* possibility to define polyrhyrhms
* possibility to let the app mute and demute randomly

## Installation

Download the .apk from f-droid then install it. Be sure to allow the usage of "Unknown sources".

## Build

Two possibilities:

    $ ant debug
    or
    $ ant release

* Debug: Need android-sdk (from google). Edit local.properties with the correct android-sdk path
* Release: Need a key. Edit ant.propreties and add your name, alis and the key password

## Documentation

Dependancies: doxygen and pandoc

    $ cd doc
    $ make reference
    $ make manual

* Reference doc: reference/html/index.html
* User manual: manual/manual.html

## Support and feedback

You can send me whatever you want at funkygoby [] mailoo.org

Metronome is supported from Android JellyBean 4.3.1 to MarshMallow 6.1. More recent Android should be fine too.

Metronome runs on older Android version but with a minor issue (see below).

### Known issue (see issue tracker)

Metronome was test on Android 4.x and 2.x. However, a weird bug happens after several start/stop cycles. Depending on Android versions, the app is either closed or doesn't produce any more sound. Android doesn't report a crash.

That being said, Metronome is available for those versions as a bonus but the resolution of this bug isn't in the roadmap.

## License

This whole project, exepted the raw files, is licensed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007. See LICENSE for more details.